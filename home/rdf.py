import rdflib
from rdflib.plugins.sparql import prepareQuery
from restaurant.settings import BASE_DIR
from SPARQLWrapper import SPARQLWrapper2

files = BASE_DIR+"/hasilscript.ttl"
ex = "http://example.org/data/"
RDF = "http://www.w3.org/1999/02/22-rdf-syntax-ns#/"

g = rdflib.Graph()
g.parse(files, format="ttl")


def generateRow(data, index):
    separator = ";"
    joined = separator.join(data[0:index])
    joined = joined.replace("http://example.org/data/", "")
    return joined


def find(subs="", city="", rating=1.0, style="", price=""):
    baseQuery = """
                SELECT DISTINCT ?s ?name ?city ?pr ?rt (group_concat(?style; separator = ", ") as ?style) ?cs
                    WHERE {{
                        ?s ex:restaurantName ?name ;
                        ex:city ?city ;
                        ex:priceRange ?pr ;
                        ex:rating ?rt ;
                        ex:cuisineStyle ?cs .
                        ?cs ex:style ?style .
                """
    endQuery = """ )
            }}
            GROUP BY ?s ?name ?city ?pr ?rt
            LIMIT 20
    """

    filt = """FILTER(?rt > {}
                && regex(lcase(str(?name)), "{}") 
                && regex(lcase(str(?city)), "{}") 
                && regex(lcase(str(?style)), "{}") 
                """

    if price != "":
        filt = filt + ' && str(?pr) = "{}"'

    query = baseQuery+filt+endQuery

    q = prepareQuery(
        query.format(rating, subs.lower(), city.lower(), style.lower(), price),
        initNs={"ex": ex}
    )

    result = []

    for data in g.query(q):
        result.append(generateRow(data, 6))
    return result


def detail(name):
    q = prepareQuery(
        """
        SELECT DISTINCT ?name ?city ?pr ?rk ?rt ?nor ?url ?idTA 
        (group_concat(?style; separator = ", ") as ?style)
        ?cs
        WHERE {{
        {} ex:restaurantName ?name ;
           ex:city ?city ;
           ex:priceRange ?pr ;
           ex:ranking ?rk ;
           ex:rating ?rt ;
           ex:numberOfReviews ?nor ;
           ex:urlTA ?url ;
           ex:idTA ?idTA ;
           ex:cuisineStyle ?cs .
           ?cs ex:style ?style .
        }}
        GROUP BY ?name ?city ?pr ?rt ?nor ?url ?idTA
        """.format(name),
        initNs={"ex": ex}
    )

    q2 = prepareQuery(
        '''
        SELECT DISTINCT ?name
        (group_concat(concat(?text,"-",?date); separator = ", ") as ?teda)
        ?rvs
        ?rv
        WHERE {{
        {} ex:restaurantName ?name ;
            ex:reviews ?rvs .
            ?rvs ex:review ?rv .
            ?rv ex:text ?text ;
                ex:date ?date
        }}
        GROUP BY ?name 
        '''.format(name),
        initNs={"ex":ex}
    )

    result = []

    for data in g.query(q):
        for review in g.query(q2):
            result.append(generateRow(data, 9)+";"+review[1])

    return result


def getCityIRI(city):
    sparql = SPARQLWrapper2("http://dbpedia.org/sparql")
    sparql.setQuery("""
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX dbo: <http://dbpedia.org/ontology/>

    SELECT DISTINCT ?city
    WHERE {{
        ?city rdf:type dbo:Location;
            rdfs:label ?label
        FILTER(lcase(str(?label)) ="{}").
    }}
    LIMIT 1
    """.format(city.lower())
    )

    for result in sparql.query().bindings:
        return result["city"].value



def findDistinctCity():
    q = prepareQuery(
        """
        SELECT DISTINCT ?city
        WHERE {
            ?s ex:city ?city .
        }
        """,
        initNs={"ex": ex}
    )

    result = []

    for data in g.query(q):
        result.append(data[0])
    return result


def findDistinctCuisineStyle():
    q = prepareQuery(
        """
        SELECT DISTINCT ?style
        WHERE {
            ?s ex:cuisineStyle ?cs .
            ?cs ex:style ?style
        }
        """,
        initNs={"ex": ex}
    )

    result = []

    for data in g.query(q):
        result.append(data[0])
    return result


def findDistinctPriceRange():
    q = prepareQuery(
        """
        SELECT DISTINCT ?price
        WHERE {
            ?s ex:priceRange ?price .
        }
        """,
        initNs={"ex": ex}
    )

    result = []

    for data in g.query(q):
        result.append(data[0])
    return result

