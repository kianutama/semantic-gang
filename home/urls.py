from django.urls import path

from home import views

urlpatterns = [
    path('', views.home, name='home'),
    path('search', views.index, name='search'),
    path('detail/<str:restaurant_id>', views.detail, name='detail')
]
