from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

from home import rdf
# Create your views here.

@csrf_exempt
def index(request):

    name = ''
    city = ''
    rating = 1.0
    price = ''
    style = ''

    if 'name' in request.POST:
        name = request.POST['name']

    if 'city' in request.POST:
        city = request.POST['city']

    if 'rating' in request.POST:
        rating = request.POST['rating']

    if 'price' in request.POST:
        price = request.POST['price']

    if 'style' in request.POST:
        style = request.POST['style']

    l= rdf.find(subs=name, city=city, rating=float(rating), price=price, style=style)

    c = []
    for data in l:
        restaurant = Restaurant()
        splitted = data.split(";")
        restaurant.id = splitted[0]
        restaurant.name = splitted[1]
        restaurant.city = splitted[2]
        restaurant.priceRange = splitted[3]
        restaurant.rating = float(splitted[4])
        restaurant.cuisineStyle = splitted[5]
        c.append(restaurant)

    return render(request, 'index.html', {'data': c, 'searchFor': name})


def home(request):
    cities = rdf.findDistinctCity()
    cuisineStyle = rdf.findDistinctCuisineStyle()
    priceRange = rdf.findDistinctPriceRange()

    return render(request, 'home.html', {'cities': cities, 'cuisineStyle': cuisineStyle, 'priceRange': priceRange})


def detail(request, restaurant_id):
    m = rdf.detail("ex:"+restaurant_id)

    c = []
    for data in m:
        print(data)
        restaurant = Restaurant()
        splitted = data.split(";")
        restaurant.name = splitted[0]
        restaurant.city = splitted[1]
        restaurant.cityIRI = rdf.getCityIRI(splitted[1])
        restaurant.priceRange = splitted[2]
        restaurant.ranking = int(float(splitted[3]))
        restaurant.rating = float(splitted[4])
        restaurant.numberOfReview = int(float(splitted[5]))
        restaurant.urlTA = splitted[6]
        restaurant.idTA = splitted[7]
        restaurant.cuisineStyle = splitted[8]

        reviews = []
        for review in splitted[9].split(", "):
            info = review.split("-")
            obj = Review()
            obj.text = info[0]
            obj.date = info[1]
            reviews.append(obj)
        restaurant.reviews = reviews
        c.append(restaurant)

    return render(request, 'detail.html', {'data': c[0]})


class Restaurant:

    def __init__(self):
        self.id = ''
        self.name = ''
        self.city = ''
        self.cityIRI = ''
        self.cuisineStyle = ''
        self.priceRange = ''
        self.rating = 0.0
        self.ranking = 0
        self.numberOfReview = 0
        self.idTA = ''
        self.urlTA = ''
        self.reviews = []


class Review:

    def __init__(self):
        self.text = ''
        self.date = ''
