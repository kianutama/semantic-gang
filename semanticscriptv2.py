import csv
import json
import ast
import re



def printChild(a, b):
    b = re.sub(r'["\']','',b)
    a = "ex:" + a + " \"" + b + "\" ;"
    return "{:45s}{:30s}".format("", a)


def printChildDot(a, b):
    b = re.sub(r'["\']','',b)
    a = "ex:" + a + " \"" + b + "\" ."
    return "{:45s}{:30s}".format("", a)


def printChild2(a, b, xsd):
    b = re.sub(r'["\']','',b)
    a = "ex:" + a + " \"" + b + "\"^^xsd:" + xsd + " ;"
    return "{:45s}{:30s}".format("", a)


def printArrayChild(a, b, c, f):
    a = "ex:" + a + " ["
    b = "ex:" + b + " \"" + c + "\""
    z = "{:45s}{:30s}".format("", a)
    f.write(z + "\n")

    c = c.replace("'", "\"")
    js = []
    try:
        js = json.loads(c)
    except:
        js = []

    for i in range(len(js)):
        s = re.sub(r'["\']','',js[i - 1])
        x = ""

        if (i + 1 != len(c)):
            x = "{:50s}{:30s}".format("", "ex:style \"" + s + "\" ;")
        else:
            x = "{:50s}{:30s}".format("", "ex:style \"" + s + "\"")
        f.write(x + "\n")

    # c = c.replace("[","")
    # c = c.replace("]","")
    # c = c.replace(" ","")
    # c = c.replace("'", "\"")
    # c = c.split(",")
    # for i in range (len(c)):
    # 	x = ""
    # 	if(c[i-1][0] != '"'):
    # 		c[i-1] = '"'+c[i-1]
    # 	if(c[i-1][len(c[i-1])-1] != '"'):
    # 		c[i-1] = c[i-1]+'"'

    # 	if(i+1 != len(c)):
    # 		x = "{:35s}{:30s}".format("","ex:style "+ c[i-1]+" ;")
    # 	else:
    # 		x = "{:35s}{:30s}".format("","ex:style "+ c[i-1])
    # 	f.write(x+"\n")
    v = "{:45s}{:30s}".format("", "] ;")
    f.write(v + "\n")


def printArrayChild2(a, b, c, f):
    a = "ex:" + a + " ["
    b = "ex:" + b + " \"" + c + "\""
    z = "{:45s}{:30s}".format("", a)
    f.write(z + "\n")

    # c = c.replace("'","\"")
    # print(c)
    # c = checkdeh(c)
    # print(c)
    # js = json.loads(c)

    if c != '':
        zzz = ast.literal_eval(c)

        for x in range(len(zzz[0])):
            s = re.sub(r'["\']','',zzz[0][x - 1])

            rev = "{:50s}{:30s}".format("", "ex:review [")
            f.write(rev + "\n")
            x1 = "{:55s}{:30s}".format("", "ex:text \"" + s + "\" ;")
            f.write(x1 + "\n")
            _a = "{:55s}{:30s}".format("", "ex:date \"" + zzz[1][x - 1] + "\"")
            f.write(_a + "\n")
            if (x + 1 != len(zzz[0])):
                w = "{:50s}{:30s}".format("", "] ;")
            else:
                w = "{:50s}{:30s}".format("", "]")
            f.write(w + "\n")

    # for x in range(len(js[0])):
    #     rev = "{:35s}{:30s}".format("", "ex:review [")
    #     f.write(rev + "\n")
    #     x1 = "{:40s}{:30s}".format("", "ex:text \"" + js[0][x - 1] + "\" ;")
    #     f.write(x1 + "\n")
    #     _a = "{:40s}{:30s}".format("", "ex:date \"" + js[1][x - 1] + "\"")
    #     f.write(_a + "\n")
    #     if (x + 1 != len(js[0])):
    #         w = "{:35s}{:30s}".format("", "] ;")
    #     else:
    #         w = "{:35s}{:30s}".format("", "]")
    #     f.write(w + "\n")

    # c = c.split("], [")
    # c[0] = c[0].replace("[", "")
    # c[1] = c[1].replace("]", "")
    # c[0] = c[0].replace("'", "\"")
    # c[1] = c[1].replace("'", "\"")

    # c0 = c[0].split(", ")
    # c1 = c[1].split(", ")

    # for x in range(len(c0)):
    # 	if(c0[x-1][0] != '"'):
    # 		c0[x-1] = '"'+c0[x-1]
    # 	if(c0[x-1][len(c0[x-1])-1] != '"'):
    # 		c0[x-1] = c0[x-1]+'"'

    # 	if(c1[x-1][0] != '"'):
    # 		c1[x-1] = '"'+c1[x-1]
    # 	if(c1[x-1][len(c1[x-1])-1] != '"'):
    # 		c1[x-1] = c1[x-1]+'"'

    # 	rev = "{:35s}{:30s}".format("", "ex:review [")
    # 	f.write(rev+"\n")
    # 	x1 = "{:40s}{:30s}".format("","ex:text "+ c0[x-1]+" ;")
    # 	f.write(x1+"\n")
    # 	_a = "{:40s}{:30s}".format("","ex:date "+ c1[x-1])
    # 	f.write(_a+"\n")
    # 	if (x+1 != len(c0)):
    # 		w = "{:35s}{:30s}".format("","] ;")
    # 	else:
    # 		w = "{:35s}{:30s}".format("","]")
    # 	f.write(w+"\n")

    v = "{:45s}{:30s}".format("", "] ;")
    f.write(v + "\n")


def checkdeh(z):
    p = z.split(", ")

    for i in range(len(p)):
        for c in range(len(p[i])):
            if (p[i][c] == '[' or p[i][c] == ']'):
                continue
            else:
                if (p[i][c] != "'"):
                    p[i] = p[i][:c] + "\"" + p[i][c:]
                break

        for c in range(len(p[i])):
            if (p[i][-1 - c] == '[' or p[i][-1 - c] == ']'):
                continue
            else:
                if (p[i][-1 - c] != "'"):
                    p[i] = p[i] + "\""
                else:
                    p[i] = p[i].replace("'", "\"")
                break

    p = ", ".join(p)
    return p


with open('cleanedData.csv') as csvfile:
    f = open("hasilscript.ttl", "w+")

    f.write("@prefix ex: <http://example.org/data/> .\n")
    f.write("@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n")
    f.write("@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .\n")
    f.write("@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .\n")

    readCSV = csv.reader(csvfile, delimiter=',')
    next(readCSV)
    for row in readCSV:
        z = "{:75s}{:30s}".format('ex:' + re.sub(r'[^\w\s]','',row[1].replace(" ","")),
                                  "ex:restaurantName \"" + re.sub(r'["\']','',row[1]) + "\" ;\n")
        f.write(z)
        f.write(printChild("city", row[2]) + "\n")
        printArrayChild("cuisineStyle", "style", row[3], f)
        if(row[4] == ''):
            f.write(printChild2("ranking", '-1.0', "double") + "\n")
        else:
            f.write(printChild2("ranking", row[4], "double") + "\n")
        if (row[5] == ''):
            f.write(printChild2("rating", '-1.0', "double") + "\n")
        else:
            f.write(printChild2("rating", row[5], "double") + "\n")
        if (row[6] == ''):
            f.write(printChild("priceRange", '-') + "\n")
        else:
            f.write(printChild("priceRange", row[6]) + "\n")
        if (row[7] == ''):
            f.write(printChild2("numberOfReviews", '-1.0', "double") + "\n")
        else:
            f.write(printChild2("numberOfReviews", row[7], "double") + "\n")
        printArrayChild2("reviews", "review", row[8], f)
        f.write(printChild("urlTA", row[9]) + "\n")
        f.write(printChildDot("idTA", row[10]) + "\n")
        f.write("\n")

    f.close()
