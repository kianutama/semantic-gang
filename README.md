### Semantic Gang
#### Restaurant Data Center

---
This is a Semantic Web class project which showing database of many restaurants around the world

To run the project simply clone/pull the project from this repo, create virtual environment(up to you), install requirements, migrate, runserver

```
pip install virtualenv
virtualenv -p python3 venv
```

Windows : 
```
venv\Scripts\activate.bat
pip install -r requirements.txt
```

Linux & Mac OS :
```
source venv/bin/activate
pip install -r requirements.txt
```

After installing requirements, run :
```
python manage.py migrate
python manage.py collectstatic --noinput
python manage.py runserver
```

This project is deployed on :

https://semantic-gang.herokuapp.com/


---

For additional data, to generate the turtle(.ttl) file just simply run

```
python semanticscriptv2.py
```

File with name hasilscript.ttl will showed up.
